package net.firesquared.hardcorenomad.tile.campcomponents;

import net.firesquared.hardcorenomad.item.ItemUpgrade.UpgradeType;
import net.firesquared.hardcorenomad.tile.TileEntityDeployableBase;

public class TileEntityBedRoll extends TileEntityDeployableBase
{

	public TileEntityBedRoll()
	{
		super(UpgradeType.BEDROLL);
	}
}
