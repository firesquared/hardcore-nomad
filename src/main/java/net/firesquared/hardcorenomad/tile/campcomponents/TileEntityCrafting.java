package net.firesquared.hardcorenomad.tile.campcomponents;

import net.firesquared.hardcorenomad.item.ItemUpgrade.UpgradeType;
import net.firesquared.hardcorenomad.tile.TileEntityDeployableBase;

public class TileEntityCrafting extends TileEntityDeployableBase
{

	public TileEntityCrafting()
	{
		super(UpgradeType.CRAFTING_TABLE);
	}
}
