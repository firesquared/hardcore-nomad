package net.firesquared.hardcorenomad.client.gui;

import net.minecraft.client.gui.GuiEnchantment;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.world.World;

public class EnchantmentGUI extends GuiEnchantment
{
	public EnchantmentGUI(InventoryPlayer par1InventoryPlayer, World par2World, int par3, int par4, int par5, String par6Str) {
		super(par1InventoryPlayer, par2World, par3, par4, par5, par6Str);
	}
}
