package net.firesquaredcore.client.gui.elements;

public interface IProgressElementDataProvider
{
	public float getProgress(ProgressElement element);
}
