package net.firesquaredcore.client.gui.widgets;

import net.firesquaredcore.client.gui.elements.IGuiElement;

public interface IWidget extends IGuiElement
{
	public abstract void update();
}
