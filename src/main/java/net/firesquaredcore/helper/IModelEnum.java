package net.firesquaredcore.helper;

public interface IModelEnum
{
	public int getRegID();
	public int getModelCount();
	public int getTextureCount();
	public String getRelativeFilepath();
}
